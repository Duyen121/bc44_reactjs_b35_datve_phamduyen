import React, { Component } from "react";
import { connect } from "react-redux";
import { changeAmount, removeShoe } from "./redux/action/shoeAction";

class CartShoe extends Component {
  render() {
    let { cart } = this.props;
    return (
      <div className="col-6 mt-3 ml-2">
        <table className="table text-center">
          <thead>
            <tr>
              <th>Name</th>
              <th>Amount</th>
              <th>Price</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleChangeAmount(item, -1);
                      }}
                      className="btn btn-dark"
                    >
                      ー
                    </button>
                    <strong className="mx-3">{item.shoeNum}</strong>
                    <button
                      onClick={() => {
                        this.props.handleChangeAmount(item, 1);
                      }}
                      className="btn btn-success"
                    >
                      ＋
                    </button>
                  </td>
                  <td>${item.price * item.shoeNum}</td>
                  <td>
                    <img
                      style={{ width: 50 }}
                      src={item.image}
                      alt={item.name}
                    />
                  </td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleRemove(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleRemove: (shoe) => {
      dispatch(removeShoe(shoe));
    },
    handleChangeAmount: (shoe, option) => {
      dispatch(changeAmount(shoe, option));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
