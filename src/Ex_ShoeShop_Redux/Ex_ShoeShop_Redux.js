import React, { Component } from "react";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import CartShoe from "./CartShoe";

export default class Ex_ShoeShop_Redux extends Component {
  render() {
    return (
      <div>
        <DetailShoe />
        <div className="row">
          <CartShoe />
          <ListShoe />
        </div>
      </div>
    );
  }
}
