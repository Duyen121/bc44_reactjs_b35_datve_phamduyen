import React, { Component } from "react";
import { connect } from "react-redux";
import { viewDetailAction, buyShoe } from "./redux/action/shoeAction";

class ItemShoe extends Component {
  render() {
    let { data } = this.props;
    let { image, name } = data;
    return (
      <div className="col-4 p-3">
        <div className="card text-center h-100">
          <img
            className="card-img-top mx-auto"
            style={{ width: 100 }}
            src={image}
            alt={name}
          />
          <div className="card-body p-1">
            <h6 className="card-title">{name}</h6>
          </div>
          <button
            onClick={() => {
              this.props.handleWatchDetail(data);
            }}
            className="btn btn-info"
          >
            Detail
          </button>
          <button
            onClick={() => {
              this.props.handleBuyShoe(data);
            }}
            className="btn btn-warning"
          >
            Add to Cart
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleWatchDetail: (shoe) => {
      dispatch(viewDetailAction(shoe));
    },
    handleBuyShoe: (shoe) => {
      dispatch(buyShoe(shoe));
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
