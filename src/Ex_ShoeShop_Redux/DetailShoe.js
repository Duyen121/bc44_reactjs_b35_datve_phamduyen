import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { name, price, description, quantity } = this.props.detail;
    return (
      <div className="p-3">
        <h3 className="text-left">Detail</h3>
        <table className="table mt-3">
          <thead>
            <tr className="text-center">
              <th className="text-left">Name</th>
              <th>Price</th>
              <th>Description</th>
              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-left" scope="row">
                {name}
              </td>
              <td className="text-center">{price}</td>
              <td>{description}</td>
              <td className="text-center">{quantity}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detailShoe,
  };
};

export default connect(mapStateToProps, null)(DetailShoe);
