import {
  VIEW_DETAIL,
  DELETE_SHOE,
  BUY_SHOE,
  CHANGE_AMOUNT,
} from "../contant/shoeConstant";

// action (object) creator
export let viewDetailAction = (shoe) => {
  return {
    type: VIEW_DETAIL,
    payload: shoe,
  };
};

export let removeShoe = (shoe) => {
  return {
    type: DELETE_SHOE,
    payload: shoe,
  };
};

export let buyShoe = (shoe) => {
  return {
    type: BUY_SHOE,
    payload: shoe,
  };
};

export let changeAmount = (shoe, option) => {
  return {
    type: CHANGE_AMOUNT,
    payload: { shoe, option },
  };
};
